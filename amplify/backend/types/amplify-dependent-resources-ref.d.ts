export type AmplifyDependentResourcesAttributes = {
  auth: {
    scarylandfront: {
      UserPoolId: 'string';
      UserPoolArn: 'string';
      UserPoolName: 'string';
      AppClientIDWeb: 'string';
      AppClientID: 'string';
      CreatedSNSRole: 'string';
    };
  };
};
