import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
   *{
       margin: 0;
       padding: 0;
       outline:0;
       box-sizing:border-box;
   }
   
   body{
    overflow: hidden;
    -ms-overflow-style: none; /* for Internet Explorer, Edge */
    scrollbar-width: none; /* for Firefox */
   }

   body::-webkit-scrollbar {
    display: none; /* for Chrome, Safari, and Opera */
   }

   #root{
       margin:0 auto;
   }
   
`;
