import { createTheme } from '@mui/material';

const theme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: '#060830'
    },
    secondary: {
      main: '#BB86FC'
    },
    text: {
      primary: '#212121',
      secondary: '#757575'
    }
  },
  typography: {
    fontFamily: 'Roboto',
    h6: {
      letterSpacing: '0.17px',
      color: '#CFCFCF',
      fontWeight: 'normal'
    },
    subtitle2: {
      color: '#9E9E9E',
      letterSpacing: '0.13px'
    }
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: { minWidth: 0 }
      }
    }
  }
});

export default theme;
