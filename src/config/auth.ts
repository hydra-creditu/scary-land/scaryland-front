// Cognito Auth
const AuthExceptions = {
  WRONG_PASSWORD: 'WRONG_PASSWORD',
  CHANGE_PASSWORD_REQUIRED: 'NEW_PASSWORD_REQUIRED',
  USER_NOT_FOUND: 'UserNotFoundException',
  NOT_AUTHORIZED: 'NotAuthorizedException',
  INVALID_PASSWORD: 'InvalidPasswordException',
  USER_ALREDY_EXISTS: 'UsernameExistsException',
  USER_NOT_CONFIRMED: 'UserNotConfirmedException',
  CHANGE_PW_LIMIT_EXCEEDED: 'LimitExceededException'
};
const AuthTokenKey = 'SCARYLAND_TOKEN';

export { AuthExceptions, AuthTokenKey };
