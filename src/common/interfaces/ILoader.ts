interface LoaderProps {
    open: boolean
}

export interface ILoader {
    children?: JSX.Element,
    props: LoaderProps
}
