export interface IFormInput {
    username?: string;
    oldpassword?: string;
    password: string;
    repeatpassword?: string;
}

export interface IUserLoginFormInput {
    username: string;
    password: string;
}
