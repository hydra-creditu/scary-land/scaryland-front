export interface IBaseFC {
  children: JSX.Element | null;
}
