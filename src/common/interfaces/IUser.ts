export type IUser = {
  id: string;
  nickname: string;
  ranking: string;
  status: string;
};
