export interface IAuth {
  authed?: boolean;
  cognitoUser?: any;
  userMustSetNewPw?: boolean;
  login?: (arg0: { username: string, password: string }) => Promise<(resolve: (arg0: string) => void, reject: () => void) => void>;
  logout?: () => Promise<void>;
  setNewPassword?: (arg0: { password: string }) => Promise<void>;
  signUpUser?: (arg0: { username:string, password: string, repassword: string }) => Promise<void>;
  changeUserPassword?: (arg0: { oldpassword?: string, password: string }) => Promise<void>;
}
