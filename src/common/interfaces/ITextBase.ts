import { TypographyProps } from '@mui/material';

export interface ITextBase extends TypographyProps {
  children?: JSX.Element | string;
}
