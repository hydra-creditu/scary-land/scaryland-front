export type IBaseState<T> = {
  data?: T | [];
  loading: boolean;
  error?: string;
};
