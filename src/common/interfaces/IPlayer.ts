// To parse this data:
//
//   import { Convert, Player } from "./file";
//
//   const player = Convert.toPlayer(json);

export interface IAvatar {
  path: string;
  uid: string;
  _id: string;
}
export interface IPlayer {
  statistics: IStatistics;
  id: string;
  nickName: string;
  avatar: IAvatar;
  email: string;
  createdAt: Date;
  updatedAt: Date;
  status: boolean;
}

export interface IStatistics {
  bestScore: string;
  bestTime: string;
  bestRanking: string;
  currentRanking: number;
  currentStatus: string;
}

// Converts JSON strings to/from your types
export class Convert {
  public static toPlayer(json: string): IPlayer {
    return JSON.parse(json);
  }

  public static playerToJson(value: IPlayer): string {
    return JSON.stringify(value);
  }
}
