import { IUser } from './IUser';
import { ITextBase } from './ITextBase';
import { ILoader } from './ILoader';

export type { IUser, ILoader, ITextBase };
