// To parse this data:
//
//   import { Convert, Player } from "./file";
//
//   const player = Convert.toPlayer(json);

export interface ILevel {
  uid: string;
  nameStatus: string;
  minScore: string;
  maxScore: string;
  logo: string;
}
