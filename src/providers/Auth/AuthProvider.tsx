import AuthContext from './AuthContext';

import { IBaseFC } from '../../common/interfaces/IBaseFC';
import useProvideAuth from '../../hooks/useProvideAuth';

const AuthProvider = ({ children }: IBaseFC): JSX.Element => {
  const auth = useProvideAuth();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
};
export default AuthProvider;
