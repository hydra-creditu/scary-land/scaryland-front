import React from 'react';
import { IAuth } from '../../common/interfaces/IAuth';

export default React.createContext<IAuth>({});
