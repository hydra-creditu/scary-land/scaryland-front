import React, { lazy, Suspense } from 'react';
import { Routes, Route, Navigate, useLocation } from 'react-router-dom';
import Amplify, { Auth } from 'aws-amplify';
import { IBaseFC } from './common/interfaces/IBaseFC';
import useAuthConsumer from './hooks/useAuthConsumer';
import { BackgroundLoader } from './components/atoms/Loader/FullScreenLoader';
import About from './screens/About';
import awsconfig from './aws-exports.js';
import { LoaderProvider } from './hooks/useLoader';
import { GlobalLoader } from './components/atoms/Loader/GlobalLoader';

const Login = lazy(() => import('./screens/Login'));
const SignUp = lazy(() => import('./screens/SignUp'));
const Dashboard = lazy(() => import('./screens/Dashboard'));
const Profile = lazy(() => import('./screens/Profile'));

const RequireAuth = ({ children }: IBaseFC): JSX.Element | null => {
  const { authed } = useAuthConsumer();
  const location = useLocation();

  return authed ? (
    children
  ) : (
    <Navigate to="/" replace state={{ path: location.pathname }} />
  );
};

Amplify.configure(awsconfig);
Auth.configure(awsconfig);

const App = (): JSX.Element => (
  <Suspense fallback={<BackgroundLoader />}>
    <LoaderProvider>
      <>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route
            path="/dashboard"
            element={
              <RequireAuth>
                <Dashboard />
              </RequireAuth>
            }
          />
          <Route
            path="/profile"
            element={
              <RequireAuth>
                <Profile />
              </RequireAuth>
            }
          />
          <Route
            path="/about"
            element={
              <RequireAuth>
                <About />
              </RequireAuth>
            }
          />
          <Route path="/signup" element={<SignUp />} />
        </Routes>
        <GlobalLoader />
      </>
    </LoaderProvider>
  </Suspense>
);

export default App;
