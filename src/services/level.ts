import { BASE_URL } from '../config/api';
import { createUrl } from '../utils/requests';

export type IRequest = {
  url: RequestInfo;
  options: RequestInit;
};

export type IParams = {
  filter?: string;
};

export const getRequestLevels = (): IRequest => ({
  url: createUrl({
    base: BASE_URL,
    path: '/generic/levels'
  }),
  options: { method: 'GET' }
});
