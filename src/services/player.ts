import { BASE_URL } from '../config/api';
import { createUrl } from '../utils/requests';

export type IRequest = {
  url: RequestInfo;
  options: RequestInit;
};

export type IParams = {
  filter?: string;
};

export const getRequestHallFamePlayers = (params: IParams): IRequest => {
  const urlParams = `${
    params.filter !== '' ? `?${new URLSearchParams(params).toString()}` : ''
  }`;

  return {
    url: createUrl({
      base: BASE_URL,
      path: `/hall-fame${urlParams}`
    }),
    options: { method: 'GET' }
  };
};
