import { render, screen, waitFor } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';

import App from './App';

test('App - routes', async () => {
  render(<App />, {
    wrapper: MemoryRouter
  });

  // add waitFor to wait for the loading screen
  // Login Page
  const elementLoginOnPage = await waitFor(() => screen.getByText(/LOGIN/i));
  expect(elementLoginOnPage).toBeInTheDocument();
  userEvent.click(screen.getByText('Sign up!'));
  // Sign Up Page
  const elementSignUpOnPage = await waitFor(() => screen.getByText(/SIGN UP/i));
  expect(elementSignUpOnPage).toBeInTheDocument();
});
