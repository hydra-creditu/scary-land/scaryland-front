import { RegisterOptions } from 'react-hook-form';

type IInputRule = RegisterOptions;

const getRequiredRules = (message: string): Partial<IInputRule> => ({
  required: { value: true, message }
});

const getEmailRules = (required = true): IInputRule => {
  const patternEmail = {
    value: /^\S+@\S+\.\S+$/,
    message: 'Invalid email Address'
  };
  return {
    required: required ? { value: true, message: 'Required' } : undefined,
    pattern: patternEmail
  };
};

const getPasswordRules = (required = true): IInputRule => {
  const passwordPattern = {
    value: /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/,
    message: 'Not match with the rules'
  };
  return {
    required: required ? { value: true, message: 'Required' } : undefined,
    minLength: { value: 8, message: 'Invalid' },
    pattern: passwordPattern
  };
};
export { getRequiredRules, getEmailRules, getPasswordRules };
