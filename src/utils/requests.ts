import { IBaseState } from '../common/interfaces/IBaseState';

type ICreateUrl = {
  base: string;
  path: string;
};

type IAction<T> =
  | { type: 'request' }
  | { type: 'success'; results: T }
  | { type: 'failure'; error: string };

export const createUrl = ({ base, path }: ICreateUrl): string =>
  `${base}${path}`;

export const createReducer = <T, >() =>
  (state: IBaseState<T>, action: IAction<T>): IBaseState<T> => {
    switch (action.type) {
      case 'request':
        return { loading: true };
      case 'success':
        return { ...state, loading: false, data: action.results };
      case 'failure':
        return { ...state, loading: false, error: action.error };
      default:
        return { loading: false, error: 'Unknown action' };
    }
  };
