import ReactDOM from 'react-dom';

import { ThemeProvider } from '@mui/material';
import { BrowserRouter } from 'react-router-dom';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import GlobalStyle from './styles/global';
import theme from './styles/theme';

import App from './App';
import { AuthProvider } from './providers/Auth';

ReactDOM.render(
  <>
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <AuthProvider>
          <App />
        </AuthProvider>
      </BrowserRouter>
    </ThemeProvider>
    <GlobalStyle />
  </>,
  document.getElementById('root')
);
