import React, { useEffect } from 'react';
import {
  Box,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  Typography,
  Modal
} from '@mui/material';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { useForm, Controller, SubmitHandler } from 'react-hook-form';
import { getRequiredRules } from '../../../utils/inputRules';
import TextField from '../../atoms/TextField/Index';

interface IFormInput {
  username: string,
  password: string;
  repeatpassword: string;
  oldpassword?: string;
}

type ChangePasswordModalProps = {
  open : boolean,
  onSubmit : SubmitHandler<IFormInput>,
  onClose?: any,
  title: string,
  error?: string,
  showOldPasswordInput?: boolean
};

const containerStyle = {
  position: 'absolute' as const,
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  display: 'flex',
  flexFlow: 'row wrap',
  justifyContent: 'center'
};

const ChangePasswordModal = (props: ChangePasswordModalProps): JSX.Element => {
  const {
    control,
    handleSubmit,
    setError,
    reset,
    formState: { errors }
  } = useForm<IFormInput>();
  const { open, onSubmit, error, title, onClose, showOldPasswordInput } = props;
  const [showPassword, setShowPassword] = React.useState(false);
  const [showOldPassword, setShowOldPassword] = React.useState(false);

  useEffect(() => {
    if (error?.length) {
      setError('repeatpassword', {
        type: 'manual',
        message: error
      });
    }
  }, [error]);
  useEffect(() => {
    if (!open) {
      reset();
    }
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="Change password"
      aria-describedby="User must change password"
    >
      <Box sx={containerStyle}>
        <Typography id="modal-modal-title" variant="h6" component="h2" textAlign="center" marginBottom={2}>
          {title}
        </Typography>
        <form
          style={{
            display: 'inherit',
            flexDirection: 'column',
            alignItems: 'center'
          }}
          onSubmit={handleSubmit(onSubmit)}
        >
          {showOldPasswordInput && <Controller
            name="oldpassword"
            control={control}
            defaultValue=""
            rules={{
              ...getRequiredRules('Required')
            }}
            render={({ field }) => (
              <FormControl sx={{ m: 1 }} variant="outlined">
                <TextField
                  onChange={field.onChange}
                  value={field.value}
                  fullWidth
                  type={showOldPassword ? 'text' : 'password'}
                  label="Old password"
                  variant="outlined"
                  error={errors.oldpassword != null}
                  helperText={errors.oldpassword && errors.oldpassword.message}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          style={{ color: '#FFFFFF61' }}
                          aria-label="toggle password visibility"
                          onClick={() => {
                            setShowOldPassword(!showOldPassword);
                          }}
                          edge="end"
                        >
                          {showOldPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </FormControl>
            )}
          />}
          <Controller
            name="password"
            control={control}
            defaultValue=""
            rules={{
              ...getRequiredRules('Required')
            }}
            render={({ field }) => (
              <FormControl sx={{ m: 1 }} variant="outlined">
                <TextField
                  onChange={field.onChange}
                  value={field.value}
                  fullWidth
                  type={showPassword ? 'text' : 'password'}
                  label="Password"
                  variant="outlined"
                  error={errors.password != null}
                  helperText={errors.password && errors.password.message}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          style={{ color: '#FFFFFF61' }}
                          aria-label="toggle password visibility"
                          onClick={() => {
                            setShowPassword(!showPassword);
                          }}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </FormControl>
            )}
          />
          <Controller
            name="repeatpassword"
            control={control}
            defaultValue=""
            rules={{
              ...getRequiredRules('Required')
            }}
            render={({ field }) => (
              <FormControl sx={{ m: 1 }} variant="outlined">
                <TextField
                  onChange={field.onChange}
                  value={field.value}
                  fullWidth
                  type={showPassword ? 'text' : 'password'}
                  label="Repeat password"
                  variant="outlined"
                  error={errors.repeatpassword != null}
                  helperText={errors.repeatpassword && errors.repeatpassword.message}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          style={{ color: '#FFFFFF61' }}
                          aria-label="toggle password visibility"
                          onClick={() => {
                            setShowPassword(!showPassword);
                          }}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </FormControl>
            )}
          />
          <FormControl sx={{ alignSelf: 'flex-end' }}>
            <Button
              type="submit"
              color="secondary"
              endIcon={<ArrowForwardIcon />}
            >
              Continue
            </Button>
          </FormControl>
        </form>
      </Box>
    </Modal>
  );
};

ChangePasswordModal.defaultProps = {
  showOldPasswordInput: false
};

export default ChangePasswordModal;
