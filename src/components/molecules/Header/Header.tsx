/* eslint-disable jsx-a11y/anchor-is-valid */
import { useState, MouseEvent } from 'react';
import Toolbar from '@mui/material/Toolbar';
import LinkMUI from '@mui/material/Link';
import {
  AppBar,
  Box,
  Divider,
  IconButton,
  Menu,
  MenuItem
} from '@mui/material';
import { Person, Logout } from '@mui/icons-material';
import styled from 'styled-components';

import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import logo from '../../../assets/scaryland-logo-xl.png';
import useAuthConsumer from '../../../hooks/useAuthConsumer';

const LogoBox = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
`;

export default function Header(): JSX.Element {
  const navigate = useNavigate();
  const location = useLocation();
  const auth = useAuthConsumer();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [profileMenuOpen, setProfileMenuOpen] = useState<boolean>(false);
  const triggerCloseProfileMenu = (): void => {
    setAnchorEl(null);
    setProfileMenuOpen(false);
  };
  const openProfileMenu = (event: MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
    setProfileMenuOpen(true);
  };
  const gotoProfilePage = (): void => {
    triggerCloseProfileMenu();
    navigate('/profile');
  };
  const logoutUser = (): void => {
    triggerCloseProfileMenu();
    if (auth.logout) {
      auth
        .logout()
        .then(() => {
          navigate('/');
        })
        .catch(() => {
          alert('Cant logout user');
        });
    }
  };
  const closeProfileMenu = (): void => {
    triggerCloseProfileMenu();
  };

  return (
    <>
      <AppBar
        position="relative"
        color="primary"
        elevation={0}
        enableColorOnDark
      >
        <Box
          position="absolute"
          width={324}
          marginX="auto"
          top={0}
          right={0}
          left={0}
          paddingRight={24}
          display={{
            xs: 'none',
            md: 'flex'
          }}
          zIndex={1}
        >
          <LogoBox>
            <img width={324} src={logo} alt="Scaryland logo" />
          </LogoBox>
        </Box>
        <Toolbar>
          <Box sx={{ display: 'flex', flexGrow: 1 }}>
            <nav>
              <LinkMUI
                component={RouterLink}
                color={
                  location.pathname === '/dashboard' ? 'secondary' : 'inherit'
                }
                variant="button"
                href="#"
                sx={{
                  my: 1,
                  mx: 1.5,
                  textDecoration:
                    location.pathname === '/dashboard' ? 'underline' : 'none'
                }}
                to="/dashboard"
              >
                Hall fame
              </LinkMUI>
              <LinkMUI
                component={RouterLink}
                color={location.pathname === '/about' ? 'secondary' : 'inherit'}
                variant="button"
                href="#"
                sx={{
                  my: 1,
                  mx: 1.5,
                  textDecoration:
                    location.pathname === '/about' ? 'underline' : 'none'
                }}
                to="/about"
              >
                About us
              </LinkMUI>
            </nav>
          </Box>
          <IconButton onClick={openProfileMenu} size="small" sx={{ ml: 2 }}>
            <Person sx={{ fontSize: 50 }} />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Menu
        open={profileMenuOpen}
        anchorEl={anchorEl}
        onClick={closeProfileMenu}
        onClose={closeProfileMenu}
        elevation={0}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
      >
        <MenuItem onClick={gotoProfilePage} disableRipple>
          <Person />
          Go to my profile
        </MenuItem>
        <Divider />
        <MenuItem onClick={logoutUser} disableRipple>
          <Logout />
          Logout
        </MenuItem>
      </Menu>
    </>
  );
}
