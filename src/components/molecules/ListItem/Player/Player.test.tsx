import { render } from '@testing-library/react';
import { IUser } from '../../../../common/interfaces';
import Player from './Player';

describe('List item player', () => {
  test('Render component', () => {
    const user: IUser = {
      id: '1',
      nickname: 'test',
      ranking: 'plata',
      status: 'online'
    };
    const { container } = render(<Player user={user} />);
    expect(container).toBeInTheDocument();
  });
});
