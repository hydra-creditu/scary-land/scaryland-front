import { Box, Typography } from '@mui/material';

import { IPlayer } from '../../../../common/interfaces/IPlayer';

const Player = ({
  id,
  nickName,
  statistics,
  avatar
}: Partial<IPlayer>): JSX.Element => (
  <Box width="inherit" height="60px" position="relative" display="flex">
    <Box
      flex={1}
      bgcolor="white"
      marginX="35px"
      paddingX="40px"
      alignSelf="center"
      height="50px"
      display="flex"
      flexDirection="row"
      alignItems="center"
      justifyContent="space-between"
    >
      <Box flex={1} display="flex" flexDirection="row" alignItems="center">
        <Typography sx={{ color: '#060830' }} variant="h6">
          {nickName}
        </Typography>
      </Box>
      <Box flex={1} display="flex" flexDirection="row" alignItems="center">
        <Typography sx={{ color: '#060830' }} variant="h6">
          {id}
        </Typography>
      </Box>
      <Box flex={1} display="flex" flexDirection="row" alignItems="center">
        <Typography sx={{ color: '#060830' }} variant="h6">
          {statistics?.currentRanking.toString()}
        </Typography>
      </Box>
    </Box>
    <Box position="absolute" left={0} top={0}>
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        width={60}
        height={60}
        bgcolor="white"
        borderRadius={35}
      >
        <img
          src={avatar?.path}
          style={{ objectFit: 'contain', height: 60 }}
          alt="path"
        />
      </Box>
    </Box>
    <Box position="absolute" right={10} sx={{ top: '7.5px' }}>
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        width={45}
        height={45}
        bgcolor="#2A2D30"
        sx={{
          border: '1px solid #F7F7F7',
          transform: 'rotate(45deg)'
        }}
      >
        <img
          src={`https://content.scaryland.xyz/status/${statistics?.currentStatus}.svg`}
          style={{
            objectFit: 'contain',
            height: 20,
            transform: 'matrix(0.6, -0.6, 0.6, 0.6, 0, 0)'
          }}
          alt="path"
        />
      </Box>
    </Box>
  </Box>
);

export default Player;
