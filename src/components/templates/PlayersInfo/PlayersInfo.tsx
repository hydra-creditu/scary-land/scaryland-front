import React from 'react';
import { IBaseFC } from '../../../common/interfaces/IBaseFC';
import { PrivateContainer } from '../../atoms/Container/ContainerGradient';
import Header from '../../molecules/Header';

const PlayersInfo = ({ children }: IBaseFC): JSX.Element => (
  <div>
    <Header />
    <PrivateContainer>{children}</PrivateContainer>
  </div>
);

export default PlayersInfo;
