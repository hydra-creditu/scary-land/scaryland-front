import styled from 'styled-components';

import Box from '@mui/material/Box';

import { IBaseFC } from '../../../common/interfaces/IBaseFC';
import waves from '../../../assets/waves.svg';
import logo from '../../../assets/scaryland-logo-xl.png';
import { PublicContainer } from '../../atoms/Container/ContainerGradient';

const WaveBox = styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
  z-index: 1;
  background-image: url(${waves});
  background-position: 0 100%;
  background-repeat: repeat-x;
`;

const RectangleBox = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  background-color: #030419;
  max-width: 382px;
  width: 382px;
  color: #fff;
`;

const LogoBox = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  margin: 6.25rem 0 3.125rem;
`;

const Authentication = ({ children }: IBaseFC): JSX.Element => (
  <PublicContainer>
    <WaveBox>
      <Box display="flex" justifyContent="center" flexDirection="row">
        <RectangleBox>
          <LogoBox>
            <img src={logo} alt="Scaryland logo" />
          </LogoBox>
          {children}
        </RectangleBox>
      </Box>
    </WaveBox>
  </PublicContainer>
);

export default Authentication;
