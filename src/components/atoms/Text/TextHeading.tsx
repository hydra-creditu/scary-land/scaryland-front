import { ITextBase } from '../../../common/interfaces/ITextBase';
import TextBase from './TextBase';

const TextHeading = ({
  children,
  variant = 'h1',
  ...props
}: ITextBase): JSX.Element => (
  <TextBase letterSpacing={0.17} variant={variant} color="#CFCFCF" {...props}>
    {children}
  </TextBase>
);

export default TextHeading;
