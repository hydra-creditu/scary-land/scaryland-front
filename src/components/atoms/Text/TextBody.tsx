import { ITextBase } from '../../../common/interfaces/ITextBase';
import TextBase from './TextBase';

const TextBody = ({ children = '' }: ITextBase): JSX.Element => (
  <TextBase variant="body1">{children}</TextBase>
);

export default TextBody;
