import { Typography } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { ITextBase } from '../../../common/interfaces/ITextBase';

const TypoTheme = createTheme({
  typography: {
    h6: {
      fontSize: 18,
      fontFamily: 'Libre Baskerville'
    }
  }
});

const TextBase = ({ children, ...props }: ITextBase): JSX.Element => (
  <ThemeProvider theme={TypoTheme}>
    <Typography {...props}>{children}</Typography>
  </ThemeProvider>
);

export default TextBase;
