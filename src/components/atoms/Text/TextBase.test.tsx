import { render, screen } from '@testing-library/react';
import TextBase from './TextBase';

describe('TextBase', () => {
  test('render component', () => {
    const textExpected = 'Hi! I am a Text Base';
    render(<TextBase>{textExpected}</TextBase>);
    expect(screen.getByText(textExpected)).toBeTruthy();
  });

  test('match heading role', () => {
    const textExpected = 'Hi! I am a Text Base';
    render(<TextBase variant="h1">{textExpected}</TextBase>);
    expect(screen.getByRole('heading')).toHaveTextContent(textExpected);
  });
});
