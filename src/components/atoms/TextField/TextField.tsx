import {
  TextField as TextFieldMUI,
  TextFieldProps,
  Theme
} from '@mui/material';
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles<Theme, TextFieldProps>(() => ({
  root: (props) => ({
    '& .MuiOutlinedInput-root': {
      color: props.style?.color || '#FFFFFF61'
    },
    '& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
      borderColor: props.style?.borderColor || '#FFFFFF61'
    },
    '&:hover .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline': {
      borderColor: props.style?.borderColor || '#FFFFFF61'
    },
    '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
      borderColor: props.style?.borderColor || '#FFFFFF61'
    }
  })
}));

const TextField = ({ error, ...props }: TextFieldProps): JSX.Element => {
  const classes = useStyles(props);
  return (
    <TextFieldMUI
      className={classes.root}
      InputLabelProps={{
        style: { color: props.style?.color || '#FFFFFF61' }
      }}
      id="outlined-basic"
      label="Outlined"
      variant="outlined"
      FormHelperTextProps={{
        style: { color: !error ? '#FFFFFF61' : 'red' }
      }}
      {...props}
    />
  );
};

export default TextField;
