import styled from 'styled-components';
import monsters from '../../../assets/monsters.svg';
import birds from '../../../assets/birds.svg';
import stars from '../../../assets/stars.svg';

const PublicContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  position: relative;
  height: 100vh;
  background: url(${monsters}) bottom center no-repeat,
    url(${birds}) top center no-repeat, url(${stars}) top left repeat,
    radial-gradient(
      circle,
      rgba(77, 201, 206, 1) 8%,
      rgba(50, 138, 150, 1) 33%,
      rgba(24, 78, 97, 1) 69%,
      rgba(0, 22, 46, 1) 95%
    );
`;

const PrivateContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  position: relative;
  height: 100vh;
  background: linear-gradient(360deg, #4dc9ce 0%, #00162e 100%) 0 0 no-repeat;
`;

export { PublicContainer, PrivateContainer };
