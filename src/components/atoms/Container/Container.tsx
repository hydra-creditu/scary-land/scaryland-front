import { Container as ContainerMUI, ContainerProps } from '@mui/material';

const Container = ({ children, ...props }: ContainerProps): JSX.Element => (
  <ContainerMUI {...props}>{children}</ContainerMUI>
);

export default Container;
