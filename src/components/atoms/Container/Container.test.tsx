import { Container } from '@mui/material';
import { render, screen } from '@testing-library/react';

describe('Container', () => {
  test('render component', () => {
    const textExpected = 'Hi! I am a Text Base into container';
    render(<Container>{textExpected}</Container>);
    expect(screen.getByText(textExpected)).toBeTruthy();
  });
});
