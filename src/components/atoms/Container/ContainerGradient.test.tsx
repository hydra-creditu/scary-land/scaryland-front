import { render } from '@testing-library/react';
import { PrivateContainer, PublicContainer } from './ContainerGradient';

describe('ContainerGradient', () => {
  describe('Rendering', () => {
    test('Private container rendering', () => {
      const { container } = render(<PrivateContainer />);
      expect(container).toBeInTheDocument();
    });
    test('Public container rendering', () => {
      const { container } = render(<PublicContainer />);
      expect(container).toBeInTheDocument();
    });
  });

  describe('Container should render appended child', () => {
    const demoText = 'DemoText';
    test('Private container should render child', () => {
      const privateContainer = render(
        <PrivateContainer>
          <h1>${demoText}</h1>
        </PrivateContainer>
      );
      expect(privateContainer.container.children.length).toBe(1);
      expect(
        privateContainer.getByText(new RegExp(demoText))
      ).toBeInTheDocument();
    });

    test('Public container should render child', () => {
      const privateContainer = render(
        <PublicContainer>
          <h1>${demoText}</h1>
        </PublicContainer>
      );
      expect(privateContainer.container.children.length).toBe(1);
      expect(
        privateContainer.getByText(new RegExp(demoText))
      ).toBeInTheDocument();
    });
  });
});
