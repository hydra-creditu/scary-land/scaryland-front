import { Button as ButtonMUI, ButtonProps } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  button: {
    '&.MuiButton-root': {
      color: 'white',
      border: '1px solid white',
      paddingLeft: '20px',
      paddingRight: '20px'
    },
    '&:hover ': {
      borderColor: 'white'
    }
  }
});

const Button = ({ children, ...props }: ButtonProps): JSX.Element => {
  const classes = useStyles();
  return (
    <ButtonMUI
      className={classes.button}
      variant="outlined"
      size="medium"
      {...props}
    >
      {children}
    </ButtonMUI>
  );
};

export default Button;
