import React from 'react';
import { CircularProgress, Backdrop } from '@mui/material';
import { UseLoader } from '../../../hooks/useLoader';

const GlobalLoader = (): JSX.Element => {
  const loaderContext = UseLoader();

  return (
    <Backdrop
      sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={loaderContext.loading}
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};

export { GlobalLoader };
