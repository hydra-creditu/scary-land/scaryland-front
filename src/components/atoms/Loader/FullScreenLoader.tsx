import React from 'react';
import { Box, CircularProgress, Backdrop } from '@mui/material';
import { PrivateContainer } from '../Container/ContainerGradient';
import { ILoader } from '../../../common/interfaces/ILoader';

const BackgroundLoader = (): JSX.Element => (
  <PrivateContainer>
    <Box
      display="flex"
      justifyContent="center"
      flexDirection="row"
    >
      <CircularProgress color="inherit" />
    </Box>
  </PrivateContainer>
);

const BackdropLoader = ({ children, props }: ILoader): JSX.Element => (
  <Backdrop
    sx={{ color: '#fff', zIndex: 999 }}
    open={props.open}
  >
    {children}
    <CircularProgress color="inherit" />
  </Backdrop>
);

export { BackgroundLoader, BackdropLoader };
