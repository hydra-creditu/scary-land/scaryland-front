import React from 'react';
import { IAuth } from '../common/interfaces/IAuth';
import { AuthContext } from '../providers/Auth';

export default (): IAuth => React.useContext<IAuth>(AuthContext);
