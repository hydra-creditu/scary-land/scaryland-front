import { createContext, useContext, useState, useMemo, Context } from 'react';

type LoaderType = {
  loading?: boolean;
  setLoading?: (loading: boolean) => boolean;
}

const LoaderContext = createContext({});

const LoaderProvider = ({ children }: { children: JSX.Element | [JSX.Element] }): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const value = useMemo(() => ({ loading, setLoading }), [loading]);
  return (
    <LoaderContext.Provider value={value}>{children}</LoaderContext.Provider>
  );
};

const UseLoader = (): any => {
  const context = useContext(LoaderContext);
  if (!context) {
    throw new Error('Error');
  }
  return context;
};

export { UseLoader, LoaderProvider };
