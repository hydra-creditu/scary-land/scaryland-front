import React from 'react';
import { Auth } from 'aws-amplify';
import { AuthExceptions, AuthTokenKey } from '../config/auth';
import { IUserLoginFormInput } from '../common/interfaces/IChangePassword';

const useProvideAuth = <T, >(): Partial<any> => {
  const [authed, setAuthed] = React.useState(false);
  const [userMustSetNewPw, setUserMustSetNewPw] = React.useState(false);
  const [cognitoUser, setCognitoUser] = React.useState({ username: '' });
  const clearUserSessionData = (): void => {
    sessionStorage.removeItem(AuthTokenKey);
    setAuthed(false);
  };

  return {
    authed,
    cognitoUser,
    /**
     * Control for change password modal
     */
    userMustSetNewPw,
    /**
     * Authenticates user using AWS Cognito
     * @param userData
     * @returns
     */
    login(userData: IUserLoginFormInput) {
      const { username, password } = userData;
      return new Promise<void>((resolve, reject) => {
        Auth.signIn(username, password).then((user) => {
          setCognitoUser(user);
          if (user.challengeName === AuthExceptions.CHANGE_PASSWORD_REQUIRED) {
            setUserMustSetNewPw(true);
            const newPasswordError = new Error('AuthError');
            newPasswordError.name = user.challengeName;
            reject(newPasswordError);
          } else {
            resolve();
            setAuthed(true);
          }
        }).catch((err) => {
          clearUserSessionData();
          reject(err);
        });
      });
    },
    /**
     * Logout user using AWS Cognito
     * @returns
     */
    logout() {
      return new Promise<void>((resolve, reject) => {
        Auth.signOut().then(() => {
          clearUserSessionData();
          resolve();
        }).catch(() => {
          reject();
        });
      });
    },
    /**
     * Change user password
     * @param passwordData
     * @returns
     */
    setNewPassword(passwordData: { password: string }) {
      const { password } = passwordData;
      return new Promise<void>((resolve, reject) => {
        Auth.completeNewPassword(
          cognitoUser,
          password,
          {
            nickname: cognitoUser.username
          }
        ).then((user) => {
          setUserMustSetNewPw(false);
          resolve(user);
        }).catch((err) => {
          setUserMustSetNewPw(true);
          reject(err);
        });
      });
    },
    /**
     * Get JWT Token from current authenticated user
     * @returns
     */
    async getAuthToken() {
      const user = await Auth.currentAuthenticatedUser();
      return user.signInUserSession.accessToken.jwtToken || '';
    },
    /**
     * Sign up new users
     * @param userData
     * @returns
     */
    signUpUser(userData: { username: string, password: string, repassword: string }) {
      const { username, password } = userData;
      return new Promise<void>((resolve, reject) => {
        Auth.signUp({
          username,
          password,
          attributes: {
            nickname: username
          }
        }).then((user) => {
          resolve();
        }).catch((err) => {
          reject(err);
        });
      });
    },
    changeUserPassword(userData: { oldpassword: string, password: string }) {
      const { oldpassword, password } = userData;
      return new Promise<void>((resolve, reject) => {
        Auth.changePassword(
          cognitoUser,
          oldpassword,
          password
        ).then((user) => {
          resolve();
        }).catch((err) => {
          reject(err);
        });
      });
    }
  };
};
export default useProvideAuth;
