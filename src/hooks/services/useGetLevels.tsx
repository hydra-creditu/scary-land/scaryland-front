import { useMemo } from 'react';
import { IBaseState } from '../../common/interfaces/IBaseState';
import { ILevel } from '../../common/interfaces/ILevel';
import { getRequestLevels } from '../../services/level';

import useApiResult from '../useApiResult';

const useGetLevels = (): IBaseState<ILevel[]> => {
  const request = useMemo(() => getRequestLevels(), []);
  return useApiResult<ILevel[]>(request);
};

export default useGetLevels;
