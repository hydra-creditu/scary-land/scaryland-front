import { useMemo } from 'react';
import { IBaseState } from '../../common/interfaces/IBaseState';
import { IPlayer } from '../../common/interfaces/IPlayer';
import { getRequestHallFamePlayers } from '../../services/player';

import useApiResult from '../useApiResult';

const useGetHallFamePlayers = (search: string): IBaseState<IPlayer[]> => {
  const request = useMemo(
    () => getRequestHallFamePlayers({ filter: search }),
    [search]
  );
  return useApiResult<IPlayer[]>(request);
};

export default useGetHallFamePlayers;
