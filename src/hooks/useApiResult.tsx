/* eslint-disable comma-spacing */
import { useEffect, useReducer } from 'react';
import { Auth } from 'aws-amplify';
import { IBaseState } from '../common/interfaces/IBaseState';

import { IRequest } from '../services/player';
import { createReducer } from '../utils/requests';

const useApiResult = <T,>(requestI: IRequest): IBaseState<T> => {
  const reducer = createReducer<T>();
  const [state, dispatch] = useReducer(reducer, {
    loading: false
  });

  const addAuthHeaders = async (
    requestOptions: RequestInit
  ): Promise<RequestInit> => {
    const user = await Auth.currentAuthenticatedUser();
    const requestHeaders = {
      ...('headers' in requestOptions ? requestOptions.headers : {}),
      ...{
        Authorization: `Bearer ${
          user.signInUserSession.accessToken.jwtToken || ''
        }`
      }
    };
    return { ...requestOptions, ...{ headers: requestHeaders } };
  };

  const makeRequest = async (request: IRequest): Promise<void> => {
    try {
      dispatch({ type: 'request' });
      const requestOptions = await addAuthHeaders(request.options);
      const response = await fetch(request.url, requestOptions);
      if (!response.ok) {
        dispatch({ type: 'failure', error: await response.text() });
      }
      const responseJson = await response.json();

      dispatch({ type: 'success', results: responseJson.body });
    } catch (e) {
      if (e instanceof Error) {
        dispatch({ type: 'failure', error: e.message });
      }
    }
  };

  useEffect(() => {
    makeRequest(requestI);
  }, [requestI]);

  return state;
};

export default useApiResult;
