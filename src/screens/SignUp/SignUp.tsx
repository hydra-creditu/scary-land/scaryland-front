import React from 'react';
import {
  Box,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  Modal,
  Typography
} from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { useForm, Controller, SubmitHandler } from 'react-hook-form';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useNavigate } from 'react-router-dom';

import TextField from '../../components/atoms/TextField/TextField';
import { TextHeading } from '../../components/atoms/Text';
import { getRequiredRules } from '../../utils/inputRules';
import Templates from '../../components/templates';
import useAuthConsumer from '../../hooks/useAuthConsumer';
import { AuthExceptions } from '../../config/auth';
import { UseLoader } from '../../hooks/useLoader';

interface IFormInput {
  username: string;
  password: string;
  repassword: string;
}

const SignUp = (): JSX.Element => {
  const navigate = useNavigate();
  const auth = useAuthConsumer();
  const loaderContext = UseLoader();
  const {
    control,
    handleSubmit,
    setError,
    formState: { errors }
  } = useForm<IFormInput>();
  const [showPassword, setShowPassword] = React.useState(false);
  const [showSuccessSignUpModal, setShowSuccessSignUpModal] = React.useState(false);
  const triggerValidationError = (message: string): void => {
    setError('repassword', {
      type: 'manual',
      message
    });
  };
  const redirectToHomepage = ():void => {
    navigate('/');
  };
  const onSubmit: SubmitHandler<IFormInput> = (newUserData) => {
    const { username, password, repassword } = newUserData;
    if (password !== repassword) {
      triggerValidationError('Passwords does not match');
    } else if (auth.signUpUser) {
      loaderContext.setLoading(true);
      auth.signUpUser(newUserData).then(() => {
        loaderContext.setLoading(false);
        setShowSuccessSignUpModal(true);
      }).catch((err) => {
        loaderContext.setLoading(false);
        const { name, message } = err;
        if (name === AuthExceptions.INVALID_PASSWORD) {
          triggerValidationError('Password too weak');
        } else if (name === AuthExceptions.USER_ALREDY_EXISTS) {
          triggerValidationError('User already exists');
        } else {
          triggerValidationError(message);
        }
      });
    }
  };

  return (
    <Templates.Authentication>
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="flex-end"
        paddingX={2}
        paddingBottom={{
          xs: 4,
          md: 8,
          lg: 20,
          xl: 35
        }}
      >
        <Box
          display="flex"
          marginY={1}
          justifyContent="center"
          flexDirection="row"
        >
          <TextHeading variant="h6">SIGN UP</TextHeading>
        </Box>
        <form
          style={{
            display: 'inherit',
            flexDirection: 'inherit'
          }}
          onSubmit={handleSubmit(onSubmit)}
        >
          <Controller
            name="username"
            control={control}
            defaultValue=""
            rules={{
              ...getRequiredRules('Required')
            }}
            render={({ field }) => (
              <FormControl sx={{ m: 1 }} variant="outlined">
                <TextField
                  onChange={field.onChange}
                  value={field.value}
                  fullWidth
                  label="Username"
                  variant="outlined"
                  error={errors.username != null}
                  helperText={errors.username && errors.username.message}
                />
              </FormControl>
            )}
          />
          <Controller
            name="password"
            control={control}
            defaultValue=""
            rules={{
              ...getRequiredRules('Required')
            }}
            render={({ field }) => (
              <FormControl sx={{ m: 1 }} variant="outlined">
                <TextField
                  onChange={field.onChange}
                  value={field.value}
                  fullWidth
                  type={showPassword ? 'text' : 'password'}
                  label="Password"
                  variant="outlined"
                  error={errors.password != null}
                  helperText={errors.password && errors.password.message}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          style={{ color: '#FFFFFF61' }}
                          aria-label="toggle password visibility"
                          onClick={() => {
                            setShowPassword(!showPassword);
                          }}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </FormControl>
            )}
          />

          <Controller
            name="repassword"
            control={control}
            defaultValue=""
            rules={{
              ...getRequiredRules('Required')
            }}
            render={({ field }) => (
              <FormControl sx={{ m: 1 }} variant="outlined">
                <TextField
                  onChange={field.onChange}
                  value={field.value}
                  fullWidth
                  type={showPassword ? 'text' : 'password'}
                  label="Password"
                  variant="outlined"
                  error={errors.repassword != null}
                  helperText={errors.repassword && errors.repassword.message}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          style={{ color: '#FFFFFF61' }}
                          aria-label="toggle password visibility"
                          onClick={() => {
                            setShowPassword(!showPassword);
                          }}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </FormControl>
            )}
          />
          <FormControl sx={{ alignSelf: 'flex-end' }}>
            <Button
              type="submit"
              color="secondary"
              endIcon={<ArrowForwardIcon />}
            >
              Continue
            </Button>
          </FormControl>
        </form>
        <Modal
          open={showSuccessSignUpModal}
          onClose={redirectToHomepage}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={{
            position: 'absolute' as const,
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
            display: 'flex',
            flexFlow: 'row wrap',
            justifyContent: 'center'
          }}
          >
            <Typography marginBottom={2} textAlign="center" variant="h6" component="h2">
              Registro exitoso, inicia sesión con tus credenciales
            </Typography>
            <Button
              type="submit"
              color="secondary"
              onClick={redirectToHomepage}
              endIcon={<ArrowForwardIcon />}
            >
              Login
            </Button>
          </Box>
        </Modal>
      </Box>
    </Templates.Authentication>
  );
};

export default SignUp;
