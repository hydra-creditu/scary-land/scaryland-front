import React from 'react';
import {
  Box,
  Button,
  Divider,
  FormControl,
  IconButton,
  InputAdornment,
  Typography
} from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { useForm, Controller, SubmitHandler } from 'react-hook-form';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { Link, useNavigate } from 'react-router-dom';

import TextField from '../../components/atoms/TextField/TextField';
import { TextHeading } from '../../components/atoms/Text';
import { getRequiredRules } from '../../utils/inputRules';
import Templates from '../../components/templates';

import useAuthConsumer from '../../hooks/useAuthConsumer';
import ChangePasswordModal from '../../components/molecules/ChangePasswordModal';
import { AuthExceptions } from '../../config/auth';
import { UseLoader } from '../../hooks/useLoader';
import {
  IFormInput,
  IUserLoginFormInput
} from '../../common/interfaces/IChangePassword';

const Login = (): JSX.Element => {
  const navigate = useNavigate();
  const auth = useAuthConsumer();
  const loaderContext = UseLoader();

  const {
    control,
    handleSubmit,
    setError,
    formState: { errors }
  } = useForm<IUserLoginFormInput>();
  const [showPassword, setShowPassword] = React.useState(false);
  const onSubmit: SubmitHandler<IUserLoginFormInput> = async (userData) => {
    if (auth.login) {
      loaderContext.setLoading(true);
      auth
        .login(userData)
        .then(() => {
          loaderContext.setLoading(false);
          navigate('/dashboard');
        })
        .catch((error) => {
          loaderContext.setLoading(false);
          const { name } = error;
          if (
            name === AuthExceptions.WRONG_PASSWORD ||
            name === AuthExceptions.USER_NOT_FOUND ||
            name === AuthExceptions.NOT_AUTHORIZED ||
            name === AuthExceptions.USER_NOT_CONFIRMED
          ) {
            setError('password', {
              type: 'manual',
              message:
                name === AuthExceptions.USER_NOT_CONFIRMED
                  ? 'User requires confirmation. Contact your admin'
                  : 'Wrong user or password'
            });
          }
        });
    }
  };

  const [newPwError, setNewPwError] = React.useState('');
  const onSubmitSetNewPassword: SubmitHandler<IFormInput> = async (
    userData
  ) => {
    const { password, repeatpassword } = userData;
    if (password !== repeatpassword) {
      setNewPwError('The passwords do not match');
    } else if (auth.setNewPassword) {
      auth
        .setNewPassword({ password })
        .then((user) => {
          console.info('PasswordOK', user);
        })
        .catch((error) => {
          const { name } = error;
          if (name === AuthExceptions.INVALID_PASSWORD) {
            setNewPwError('Password too weak');
          }
          console.info('ErrorChangePw', error);
        });
    }
  };

  return (
    <Templates.Authentication>
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="flex-end"
        paddingX={2}
      >
        <Box
          display="flex"
          marginY={1}
          justifyContent="center"
          flexDirection="row"
        >
          <TextHeading variant="h6">LOGIN</TextHeading>
        </Box>
        <form
          style={{
            display: 'inherit',
            flexDirection: 'inherit'
          }}
          onSubmit={handleSubmit(onSubmit)}
        >
          <Controller
            name="username"
            control={control}
            defaultValue=""
            rules={{
              ...getRequiredRules('Required')
            }}
            render={({ field }) => (
              <FormControl sx={{ m: 1 }} variant="outlined">
                <TextField
                  onChange={field.onChange}
                  value={field.value}
                  fullWidth
                  label="Username"
                  variant="outlined"
                  error={errors.username != null}
                  helperText={errors.username && errors.username.message}
                />
              </FormControl>
            )}
          />
          <Controller
            name="password"
            control={control}
            defaultValue=""
            rules={{
              ...getRequiredRules('Required')
            }}
            render={({ field }) => (
              <FormControl sx={{ m: 1 }} variant="outlined">
                <TextField
                  onChange={field.onChange}
                  value={field.value}
                  fullWidth
                  type={showPassword ? 'text' : 'password'}
                  label="Password"
                  variant="outlined"
                  error={errors.password != null}
                  helperText={errors.password && errors.password.message}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          style={{ color: '#FFFFFF61' }}
                          aria-label="toggle password visibility"
                          onClick={() => {
                            setShowPassword(!showPassword);
                          }}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                />
              </FormControl>
            )}
          />
          <FormControl sx={{ alignSelf: 'flex-end' }}>
            <Button
              type="submit"
              color="secondary"
              endIcon={<ArrowForwardIcon />}
            >
              Continue
            </Button>
          </FormControl>
        </form>
        <Box
          height="10rem"
          display="flex"
          justifyContent="center"
          flexDirection="row"
          marginTop={{
            xs: 15,
            md: 5,
            lg: 15,
            xl: 24
          }}
        >
          <Typography component="span" variant="subtitle2">
            {` Don't have an account${','}`}
          </Typography>
          <Divider style={{ width: 5 }} />
          <Link to="/signup" style={{ color: '#FFFFFF61' }}>
            <Typography component="span" variant="subtitle2">
              Sign up!
            </Typography>
          </Link>
        </Box>
        <ChangePasswordModal
          title="You must change your password"
          open={Boolean(auth.userMustSetNewPw)}
          onSubmit={onSubmitSetNewPassword}
          error={newPwError}
        />
      </Box>
    </Templates.Authentication>
  );
};

export default Login;
