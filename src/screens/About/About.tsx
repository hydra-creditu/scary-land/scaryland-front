import { Box, Divider, Typography } from '@mui/material';
import React from 'react';

import { PublicContainer } from '../../components/atoms/Container/ContainerGradient';
import Header from '../../components/molecules/Header';

const About = (): JSX.Element => (
  <>
    <Header />
    <PublicContainer>
      <Box width="80%" bgcolor="rgba(255, 255, 255, 0.1)">
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          p={10}
        >
          <Typography color="white" variant="h4">
            About us
          </Typography>
          <Box mt={5}>
            <Typography color="white" variant="body1" letterSpacing={0.15}>
              {`The player flies around a little black creature called a Clone
            through the woods of the game. Beginning in Day I, the game
            progresses through four stages which consist of Dawn, Noon, Dusk,
            and Night, each with a separate color scheme and new theme of traps.
            As the player goes through Day I, egg-shaped machines begin to come
            out of the water of the background. Heading into the night, the
            machines begin to turn on and the game becomes harder as the
            machines become part of the dangers of the forest. `}
            </Typography>
          </Box>
          <Box mt={5}>
            <Typography color="white" variant="body1" letterSpacing={0.15}>
              {`Eventually, the  character succeeds in disabling the machines and the machines become
            dormant once again until Day II begins it all over. Most of Day II
            is similar in plot structure, but smaller octopus-like machines
            begin to make themselves known and the animals in the forest begin
            to disappear. The last level includes saving a rabbit that was
            hanging by its foot and eventually coming to fly in front of a giant
            eye of one of the machines, leaving a cliffhanger into possible
            future updates. Also included in the game are possible in-app
            purchases that continue the story with some of Clony's friends,
            including ones nicknamed Snorf and Fury, featured in their own level
            packs. In early 2014, level pack Doomsday was released, featuring
            Fury and his escape from the machine infested woods. Also included
            is the Daydream level pack in which Snorf escapes finding the magic
            of the forest makes the woods not all as it seems to be. In 2015 a
            new multiplayer feature was released which allowed up to 4 players
            to play at the same time.`}
            </Typography>
          </Box>
        </Box>
      </Box>
    </PublicContainer>
  </>
);

export default About;
