import React from 'react';
import { SubmitHandler } from 'react-hook-form';
import { IFormInput } from '../../common/interfaces/IChangePassword';
import ChangePasswordModal from '../../components/molecules/ChangePasswordModal';
import { AuthExceptions } from '../../config/auth';
import useAuthConsumer from '../../hooks/useAuthConsumer';

const ChangePassword = (props: { open: boolean, onClose: any }): JSX.Element => {
  const { open, onClose } = props;
  const auth = useAuthConsumer();
  const [newPwError, setNewPwError] = React.useState('');

  const onSubmit: SubmitHandler<IFormInput> = async (userData) => {
    const { oldpassword, password, repeatpassword } = userData;
    if (password !== repeatpassword) {
      setNewPwError('The passwords do not match');
    } else if (auth.changeUserPassword) {
      auth.changeUserPassword({ oldpassword, password }).then((user) => {
        onClose();
        console.log('PasswordOK', user);
      }).catch((error) => {
        const { name } = error;
        if (name === AuthExceptions.INVALID_PASSWORD) {
          setNewPwError('Password too weak');
        } else if (name === AuthExceptions.CHANGE_PW_LIMIT_EXCEEDED) {
          setNewPwError('Attempt limit exceeded, please try after some time.');
        } else {
          setNewPwError('Error');
        }
      });
    }
  };

  return (
    <ChangePasswordModal
      title="Change password"
      open={open}
      onSubmit={onSubmit}
      onClose={onClose}
      error={newPwError}
      showOldPasswordInput
    />
  );
};

export default ChangePassword;
