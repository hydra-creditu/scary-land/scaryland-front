import React, { useEffect, useState } from 'react';
import { Box, IconButton } from '@mui/material';
import styled from 'styled-components';
import AccessTime from '@mui/icons-material/AccessTime';
import SportsScoreIcon from '@mui/icons-material/SportsScore';
import StarsIcon from '@mui/icons-material/Stars';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import PlayersInfo from '../../components/templates/PlayersInfo';
import { TextHeading } from '../../components/atoms/Text';
import TextBase from '../../components/atoms/Text/TextBase';
import SilverStar from '../../assets/silver-star.png';
import useAuthConsumer from '../../hooks/useAuthConsumer';
import useGetHallFamePlayers from '../../hooks/services/useGetHallFamePlayers';
import { IAvatar, IPlayer } from '../../common/interfaces/IPlayer';
import { STATIC_RESOURCES_URL } from '../../config/api';
import Button from '../../components/atoms/Button';
import { UseLoader } from '../../hooks/useLoader';
import ChangePassword from './ChangePassword';
import useGetLevels from '../../hooks/services/useGetLevels';
import { ILevel } from '../../common/interfaces/ILevel';

const StarImage = styled.div`
  background-image: url(${SilverStar});
  width: 30px;
  height: 30px;
  position: absolute;
  right: -15px;
  bottom: -7px;
`;

const ProfileContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  border: 1px solid #ffffff;
  color: #ffffff;
  margin-top: 3rem;
`;

const ProfileImage = styled.img`
  width: 140px;
  margin-top: -44px;
  min-height: 140px;
`;

const defaultPlayer = {
  id: '',
  nickName: '',
  email: '',
  avatar: { path: '' } as IAvatar,
  statistics: { bestTime: '-', bestScore: '-', bestRanking: '-', currentStatus: 'B1', currentRanking: 0 },
  createdAt: new Date(),
  updatedAt: new Date(),
  status: false
};

const GameProgressBar = (props: {
  bestScore: string;
  status: string;
  nextLevelScore: string | number;
}): JSX.Element => {
  const barColors: { readonly [index: string]: string } = {
    B: 'linear-gradient(270deg, #915824 0%, #d7b681 8%, #915824 100%)',
    P: 'linear-gradient(270deg, #626C72 0%, #F7F7F7 8%, #808080 100%)',
    O: 'linear-gradient(270deg, #ad9331 0%, #efe994 8%, #ad9331 100%)',
    default: '#C8C8C8'
  };
  const { bestScore, status, nextLevelScore } = props;
  const color = status ? status.substr(0, 1) : '';
  return (
    <Box
      width="100%"
      display="flex"
      flexDirection="row"
      marginTop={2}
      sx={{
        backgroundColor: 'rgba(255, 255, 255, 0.2)'
      }}
    >
      <Box
        width={`${((Number(bestScore) * 100) / Number(nextLevelScore)).toFixed(2)}%`}
        sx={{
          background: color in barColors ? barColors[color] : barColors.default,
          position: 'relative',
          height: '1rem'
        }}
      >
        <StarImage />
      </Box>
    </Box>
  );
};

const PlayerStatus = (props: {
  status: string;
  logo?: string;
}): JSX.Element => {
  const { status, logo } = props;
  const statusString = ''.concat(status).replace(/(O|B|P)(\d+)/gi, (string, letter, number) => {
    const letterMap: { [index: string]: string } = { O: 'Oro', P: 'Plata', B: 'Bronce' };
    return `${letterMap[letter]} ${'I'.repeat(number)}`;
  });
  return (
    <Box
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <img width="90" src={logo || `${STATIC_RESOURCES_URL}/status/${status}.svg`} alt={status} />
      <TextBase fontSize={20} textAlign="center" marginTop={1}>
        {statusString}
      </TextBase>
    </Box>
  );
};

const IconInfo = (props: {
  icon: JSX.Element;
  value: string;
  description: string;
}): JSX.Element => {
  const { icon, value, description } = props;
  return (
    <Box display="flex" color="white">
      <Box display="flex" alignItems="center" marginRight={1}>
        {icon}
      </Box>
      <Box display="flex" flexDirection="column">
        <TextBase fontSize={40} lineHeight={1}>
          {value}
        </TextBase>
        <TextBase fontSize={16}>{description}</TextBase>
      </Box>
    </Box>
  );
};

const calcNextLevel = (levelsData: ILevel[] | [] | undefined, bestScore: number): ILevel => {
  const nextLevel = levelsData?.filter((item) => (
    !Number.isNaN(bestScore) && Number(item.minScore) <= bestScore && Number(item.maxScore) >= bestScore
  ));

  return nextLevel?.length ? nextLevel[0] : {
    uid: '',
    nameStatus: '',
    minScore: '',
    maxScore: '',
    logo: ''
  };
};

const Profile = (props: { showBackButton?: boolean }): JSX.Element => {
  const { showBackButton } = props;
  const [changePassword, setChangePassword] = useState(false);
  const auth = useAuthConsumer();
  const loaderContext = UseLoader();
  const { username } = auth.cognitoUser;
  const { data: hallFameData } = useGetHallFamePlayers(username);
  const playerData: IPlayer = hallFameData?.length ? hallFameData[0] : defaultPlayer as IPlayer;
  const { data: levelsData } = useGetLevels();
  const nextLevel = calcNextLevel(levelsData, Number(playerData.statistics.bestScore));
  console.log('Nextlevel', nextLevel);
  useEffect(() => {
    loaderContext.setLoading(hallFameData === undefined || hallFameData?.length === 0);
  }, [hallFameData]);
  return (
    <PlayersInfo>
      <Box width={{ lg: 1000 }} display="flex" flexDirection="column">
        <Box
          width="100%"
          display="flex"
          alignItems="center"
          justifyContent={showBackButton ? 'space-between' : 'center'}
          marginTop={10}
        >
          {showBackButton && (
            <IconButton aria-label="delete">
              <ArrowBackIcon />
            </IconButton>
          )}
          <TextHeading variant="h6">MY PROFILE</TextHeading>
        </Box>
        <ProfileContainer>
          <Box
            display="flex"
            flexDirection="column"
            textAlign="center"
            paddingLeft={1}
          >
            {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
            <ProfileImage src={playerData.avatar.path} alt="Profile Image" />
            <TextBase marginTop={2}>{username}</TextBase>
          </Box>
          <Box
            display="flex"
            flexDirection="row"
            justifyContent="space-between"
            alignItems="center"
            flex="1 0 auto"
            paddingLeft={5}
            paddingRight={5}
          >
            <Box display="flex" flexDirection="column">
              <PlayerStatus
                status={playerData.statistics.currentStatus}
              />
            </Box>
            <Box display="flex" flexDirection="column">
              <TextBase fontSize={30}>
                {`${playerData.statistics.bestScore} / ${nextLevel.maxScore}`}
              </TextBase>
            </Box>
            <Box display="flex" flexDirection="column">
              <PlayerStatus
                status={nextLevel.nameStatus}
                logo={nextLevel.logo}
              />
            </Box>
          </Box>
          <GameProgressBar
            nextLevelScore={nextLevel.maxScore}
            bestScore={playerData.statistics.bestScore}
            status={playerData.statistics.currentStatus}
          />
        </ProfileContainer>
        <Box
          display="flex"
          flexDirection="row"
          justifyContent="space-around"
          marginTop={8}
        >
          <IconInfo
            icon={<AccessTime sx={{ fontSize: 40 }} />}
            description="MEJOR TIEMPO"
            value={`${playerData.statistics.bestTime}s`}
          />
          <IconInfo
            icon={<SportsScoreIcon sx={{ fontSize: 40 }} />}
            description="MEJOR NIVEL"
            value={playerData.statistics.bestScore}
          />
          <IconInfo
            icon={<StarsIcon sx={{ fontSize: 40 }} />}
            description="MEJOR RANKING"
            value={playerData.statistics.bestRanking}
          />
        </Box>
        <Box
          display="flex"
          flexDirection="row"
          justifyContent="flex-end"
          marginTop={8}
        >
          <Button
            variant="outlined"
            type="submit"
            endIcon={<EditIcon />}
            onClick={() => {
              setChangePassword(true);
            }}
          >
            CAMBIAR CONTRASEÑA
          </Button>
        </Box>
        <ChangePassword
          open={changePassword}
          onClose={() => {
            setChangePassword(false);
          }}
        />
      </Box>
    </PlayersInfo>
  );
};

export default Profile;
