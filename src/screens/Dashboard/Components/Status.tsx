import { Box, Grid } from '@mui/material';
import { memo, useState } from 'react';
import { ILevel } from '../../../common/interfaces/ILevel';
import Button from '../../../components/atoms/Button';
import useGetLevels from '../../../hooks/services/useGetLevels';

type IStatusProps = {
  onCancel: () => void;
  onFilter: (status: Partial<ILevel>) => void;
};

const Status = ({ onCancel, onFilter }: IStatusProps): JSX.Element => {
  const state = useGetLevels();

  const [levelItem, setLevelItem] = useState<Partial<ILevel>>({ uid: '' });
  if (state.loading && state.data?.length === 0) {
    return <div>Loading...</div>;
  }

  if (state.data === undefined) {
    return <div />;
  }

  return (
    <Box
      display="flex"
      justifyContent="center"
      flexDirection="column"
      paddingX={{ xs: 2, sm: 8, md: 12, lg: 16, xl: 20 }}
    >
      <Grid container spacing={2}>
        {state.data.map((item, index) => {
          const { logo, uid } = item;

          const indexKey = index.toString();

          return (
            <Grid
              key={`${uid}-${indexKey}`}
              display="flex"
              justifyContent="center"
              alignItems="center"
              item
              xs={4}
            >
              <Button
                variant="text"
                className={undefined}
                onClick={() => setLevelItem(item)}
                style={
                  levelItem.uid === item.uid
                    ? { borderBottom: '2px solid #00162E' }
                    : {}
                }
              >
                <img width={70} src={logo} alt={`alt-${logo}`} />
              </Button>
            </Grid>
          );
        })}
      </Grid>
      <Box display="flex" justifyContent="space-evenly" mt={4}>
        <Button onClick={onCancel}>Cancelar</Button>
        <Button onClick={() => onFilter(levelItem)}>Filtrar</Button>
      </Box>
    </Box>
  );
};
export default memo(Status);
