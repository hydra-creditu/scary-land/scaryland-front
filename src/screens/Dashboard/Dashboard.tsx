/* eslint-disable @typescript-eslint/prefer-as-const */
import {
  Box,
  FormControl,
  IconButton,
  InputAdornment,
  Modal,
  Typography
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import _ from 'lodash';
import { memo, useState } from 'react';
import { TextHeading } from '../../components/atoms/Text';

import TextField from '../../components/atoms/TextField/TextField';
import PlayersInfo from '../../components/templates/PlayersInfo';

import ListItem from '../../components/molecules/ListItem';
import useGetHallFamePlayers from '../../hooks/services/useGetHallFamePlayers';
import Button from '../../components/atoms/Button';
import Status from './Components/Status';
import { UseLoader } from '../../hooks/useLoader';

const HallFameContainer = ({ search }: { search: string }): JSX.Element => {
  const loaderContext = UseLoader();
  loaderContext.setLoading(true);
  const { data, loading } = useGetHallFamePlayers(search);
  if (!loading) {
    loaderContext.setLoading(false);
  }

  return (
    <Box>
      <Box position="relative" width="inherit">
        <Box
          marginX="35px"
          paddingX="40px"
          display="flex"
          flexDirection="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <Box flex={1} display="flex" flexDirection="row" alignItems="center">
            <Typography color="white" variant="h6">
              NICKNAME
            </Typography>
          </Box>
          <Box flex={1} display="flex" flexDirection="row" alignItems="center">
            <Typography color="white" variant="h6">
              ID
            </Typography>
          </Box>
          <Box flex={1} display="flex" flexDirection="row" alignItems="center">
            <Typography color="white" variant="h6">
              Ranking
            </Typography>
          </Box>
        </Box>
        <Box position="absolute" right={10} top={0}>
          <Typography color="white" variant="h6">
            Status
          </Typography>
        </Box>
      </Box>
      <div style={{ height: 'calc(100vh - 280px)', overflow: 'auto' }}>
        {data &&
          data.map((player) => (
            <Box key={player.id.toString()} my={2}>
              <ListItem.Player {...player} />
            </Box>
          ))}
      </div>
    </Box>
  );
};

const Dashboard = (): JSX.Element => {
  const [search, setSearch] = useState<string>('');
  const [open, setOpen] = useState(false);
  const handleOpen = (): void => setOpen(true);

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setSearch(e.target.value);
  };

  const deboundedSearch = _.debounce(handleSearch, 500);

  const ModalStatus = memo(() => {
    const handleClose = (): void => setOpen(false);
    return (
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            position: 'absolute' as 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: { xs: '90%', sm: '70%', md: '60%', lg: 500, xl: 700 },
            borderRadius: 1,
            bgcolor: '#3F4F74',
            p: 4,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column'
          }}
        >
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{ color: 'white' }}
          >
            FILTER
          </Typography>
          <Typography
            id="modal-modal-description"
            variant="subtitle2"
            sx={{ mt: 1, color: 'white', letterSpacing: '0.13px' }}
          >
            Elije el status por el o los status por los cuales deseas filtrar
          </Typography>
          <Status
            onCancel={handleClose}
            onFilter={(filter) => {
              handleClose();
              setSearch(filter?.uid ?? '');
            }}
          />
        </Box>
      </Modal>
    );
  });

  return (
    <PlayersInfo>
      <Box width={{ xs: 400, sm: 600, md: 600, lg: 800, xl: 1000 }}>
        <Box width="100%" mt={10} mb={4} display="flex" alignItems="center">
          <TextHeading variant="h5">HALL OF FAME</TextHeading>
          <Box
            flex={1}
            display="flex"
            flexDirection="row"
            justifyContent="flex-end"
            alignItems="center"
          >
            <FormControl sx={{ m: 1 }} variant="outlined">
              <TextField
                onChange={deboundedSearch}
                size="small"
                fullWidth
                label="Search"
                style={{ color: 'white', borderColor: 'white' }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        style={{ color: 'white' }}
                        aria-label="toggle password visibility"
                        edge="end"
                        onClick={() => setSearch(search)}
                      >
                        <SearchIcon />
                      </IconButton>
                    </InputAdornment>
                  )
                }}
              />
            </FormControl>

            <Button
              onClick={handleOpen}
              endIcon={<FilterAltIcon fontSize="small" />}
            >
              FILTER
            </Button>
          </Box>
        </Box>
        <HallFameContainer search={search} />
        <ModalStatus />
      </Box>
    </PlayersInfo>
  );
};

export default Dashboard;
